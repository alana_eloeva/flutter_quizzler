import 'package:flutter/material.dart';
import 'package:quizzler2/quiz_brain.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      backgroundColor: Colors.black,
      body: QuizPage(),
    ),
  ));
}

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {

  List<Widget> answerList = [];
  QuizBrain quizBrain = QuizBrain();

  void checkAnswer(bool selectedAnswer) {
    setState(() {
      if (quizBrain.isFinished) {
        quizBrain.reset();
        answerList = [];
        print("finished");
      } else {
        bool correctAnswer = quizBrain.getCorrectAnswer();
        if (selectedAnswer == correctAnswer) {
          answerList.add(Icon(Icons.check, color: Colors.green));
        } else {
          answerList.add(Icon(Icons.close, color: Colors.red));
        }
        quizBrain.nextQuestion();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  quizBrain.getQuestion(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                onPressed: () {
                  checkAnswer(true);
                },
                child: Text("True",
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                color: Colors.green,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                onPressed: () {
                  checkAnswer(false);
                },
                child: Text(
                  "False",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                color: Colors.red,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: answerList),
          )
        ],
      ),
    );
  }
}
